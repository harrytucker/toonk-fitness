# Generated by Django 2.2.1 on 2019-05-11 20:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0031_auto_20190511_2054'),
    ]

    operations = [
        migrations.RenameField(
            model_name='exercise',
            old_name='calorie_burn_rate',
            new_name='calorie_burn_rate_per_minute',
        ),
        migrations.RenameField(
            model_name='historicalexercise',
            old_name='calorie_burn_rate',
            new_name='calorie_burn_rate_per_minute',
        ),
    ]
