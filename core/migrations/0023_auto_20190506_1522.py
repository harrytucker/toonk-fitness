# Generated by Django 2.2.1 on 2019-05-06 15:22

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_auto_20190328_1237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exercise',
            name='date_time_added',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='historicalexercise',
            name='date_time_added',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
