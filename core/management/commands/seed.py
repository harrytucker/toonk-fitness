from core.models import User, Food, Drink, Meal, Profile, WeightGoal, Goal, ExerciseGoal, ExerciseType, Exercise, Group
from django.core.management.base import BaseCommand
from faker import Faker
from random import Random
import datetime
import pytz

fake = Faker()
rand = Random()

exercise_types = []

timezone = pytz.timezone("GMT")


class Command(BaseCommand):
    help = "seed database for testing and development."

    def add_arguments(self, parser):
        parser.add_argument('--mode', type=str, help="Mode")

    def handle(self, *args, **options):
        self.stdout.write('seeding data...')
        run_seed(self, options['mode'])
        self.stdout.write('done.')


def clear_data():
    """Deletes all the table data"""
    """Not Yet implemented"""
    return


def run_seed(self, mode):
    """ Seed database based on mode

    :param mode: refresh / clear
    :return:
    """
    # Clear data from tables
    clear_data()
    if mode == "clear":
        return

    # Creating pre defined exercise type list
    global exercise_types
    exercise_types = create_fake_exercise_type()

    # Creating 10 groups with users
    for i in range(10):
        self.stdout.write("group %s of %s" % (i+1, 10))
        create_fake_group()


# Create fake user objects
def create_fake_user():
    user = User.objects.create(first_name=fake.first_name(),
                               last_name=fake.last_name(),
                               username=fake.user_name(),
                               email=fake.email(),
                               password=fake.password())

    return user


# Create fake profile objects
def create_fake_profile():
    user = create_fake_user()
    weight = rand.randint(30, 200)
    height = float(rand.randint(50, 250)) / 100.0
    age = rand.randint(5, 80)
    bmi = weight / height ** 2
    gender = fake.word(['M', 'F'])
    foods = []
    for i in range(rand.randint(2, 10)):
        foods.append(create_fake_food())
    drinks = []
    for i in range(rand.randint(2, 10)):
        drinks.append(create_fake_drink())
    meals = []
    for i in range(20):
        meals.append(create_fake_meal(20-i))
    goals = []
    for i in range(rand.randint(2, 10)):
        goals.append(create_fake_weight_goal())
    for i in range(rand.randint(2, 10)):
        goals.append(create_fake_exercise_goal())
    exercises = []
    for i in range(20):
        exercises.append(create_fake_exercise(20-i))

    profile = Profile.objects.create(user=user,
                                     weight_kg=weight,
                                     height_m=height,
                                     age=age,
                                     bmi=bmi,
                                     gender=gender,
                                     )

    profile.drinks.set(drinks)
    profile.foods.set(foods)
    profile.meals.set(meals)
    profile.exercises.set(exercises)
    profile.goals.set(goals)

    return profile


# Create fake food objects
def create_fake_food():
    food = Food.objects.create(food_name=fake.sentence(nb_words=3, variable_nb_words=False),
                               calories_per_100g=rand.randint(50, 500))
    return food


# Create fake drink objects
def create_fake_drink():
    drink = Drink.objects.create(drink_name=fake.sentence(nb_words=3, variable_nb_words=False),
                                 calories_per_100ml=rand.randint(50, 500))
    return drink


# Create fake meal objects
def create_fake_meal(min_time):
    food = create_fake_food()
    drink = create_fake_drink()
    food_amount = rand.randint(50, 500)
    drink_amount = rand.randint(50, 500)
    calories = food_amount * food.calories_per_100g / 100 + drink_amount * drink.calories_per_100ml / 100
    meal_name = fake.sentence(nb_words=3, variable_nb_words=False)
    meal_of_the_day = fake.word(["SNA", "BRE", "LUN", "DIN"])
    date_time_added = fake.date_time_between(start_date=("-" + str(min_time) + "d"), end_date=("-" + str(min_time + 1) + "d"), tzinfo=timezone)

    meal = Meal.objects.create(meal_name=meal_name,
                               meal_of_the_day=meal_of_the_day,
                               food_amount_grams=drink_amount,
                               drink_amount_mls=food_amount,
                               calories=calories,
                               date_time_added=date_time_added,
                               drink=drink,
                               food=food)

    return meal


# Create fake weight goal
def create_fake_weight_goal():
    goal_name = fake.sentence(nb_words=3, variable_nb_words=False)

    start_date = fake.past_datetime(start_date="-30d", tzinfo=timezone)
    is_finished = fake.boolean()
    is_met = False
    if is_finished:
        target_date = fake.date_time_between_dates(datetime_start=start_date, datetime_end=datetime.datetime.now(),
                                                   tzinfo=timezone)
        is_met = fake.boolean()
    else:
        target_date = fake.future_datetime(end_date="+30d", tzinfo=timezone)

    target_weight = rand.randint(30, 250)
    weight_goal = WeightGoal.objects.create(target_weight=target_weight, need_to_gain_weight=fake.boolean())
    goal = Goal.objects.create(weight_goal=weight_goal,
                               goal_name=goal_name,
                               start_date=start_date,
                               target_date=target_date,
                               is_finished=is_finished,
                               is_met=is_met)

    return goal


# Create fake exercise goal
def create_fake_exercise_goal():
    name = fake.sentence(nb_words=3, variable_nb_words=False)

    start_date = fake.past_datetime(start_date="-30d", tzinfo=timezone)
    is_finished = fake.boolean()
    is_met = False
    if is_finished:
        target_date = fake.date_time_between_dates(datetime_start=start_date, datetime_end=datetime.datetime.now(),
                                                   tzinfo=timezone)
        is_met = fake.boolean()
    else:
        target_date = fake.future_datetime(end_date="+30d", tzinfo=timezone)

    exercise = create_fake_exercise(10)
    exercise_goal = ExerciseGoal.objects.create(exercise=exercise)
    goal = Goal.objects.create(exercise_goal=exercise_goal,
                               goal_name=name,
                               start_date=start_date,
                               target_date=target_date,
                               is_finished=is_finished,
                               is_met=is_met,
                               type_of_exercise=exercise.type_of_exercise)

    return goal


# Create fake exercise
def create_fake_exercise(min_time):
    exercise_name = fake.sentence(nb_words=3, variable_nb_words=False)
    type_of_exercise = exercise_types[rand.randint(0, len(exercise_types) - 1)]
    d = fake.time_object()
    duration = datetime.timedelta(hours=d.hour, minutes=d.minute, seconds=d.second, microseconds=d.microsecond)
    distance = rand.randint(250, 30000)
    date_time_added = fake.date_time_between(start_date=("-" + str(min_time) + "d"), end_date=("-" + str(min_time + 1) + "d"), tzinfo=timezone)

    exercise = Exercise.objects.create(exercise_name=exercise_name,
                                       type_of_exercise=type_of_exercise,
                                       duration=duration,
                                       distance=distance,
                                       date_time_added=date_time_added)

    return exercise


# Create fake exercise type
def create_fake_exercise_type():
    types = []

    types.append(ExerciseType.objects.create(exercise_type_name="Exercise", has_distance=False, has_duration=True,
                                             calorie_burn_rate_per_minute=10))
    types.append(ExerciseType.objects.create(exercise_type_name="Running", has_distance=True, has_duration=True,
                                             calorie_burn_rate_per_minute=12))
    types.append(ExerciseType.objects.create(exercise_type_name="Swimming", has_distance=True, has_duration=True,
                                             calorie_burn_rate_per_minute=8))
    types.append(ExerciseType.objects.create(exercise_type_name="Weight lifting", has_distance=False, has_duration=True,
                                             calorie_burn_rate_per_minute=5))
    types.append(ExerciseType.objects.create(exercise_type_name="Tennis", has_distance=False, has_duration=True,
                                             calorie_burn_rate_per_minute=7))
    types.append(ExerciseType.objects.create(exercise_type_name="Basketball", has_distance=False, has_duration=True,
                                             calorie_burn_rate_per_minute=7))
    types.append(ExerciseType.objects.create(exercise_type_name="Rowing", has_distance=True, has_duration=True,
                                             calorie_burn_rate_per_minute=10))
    types.append(
        ExerciseType.objects.create(exercise_type_name="Circuit training", has_distance=False, has_duration=True,
                                    calorie_burn_rate_per_minute=14))
    types.append(ExerciseType.objects.create(exercise_type_name="Cycling", has_distance=True, has_duration=True,
                                             calorie_burn_rate_per_minute=8))
    types.append(ExerciseType.objects.create(exercise_type_name="Walking", has_distance=True, has_duration=True,
                                             calorie_burn_rate_per_minute=4))

    return types


# Create fake group with no users
def create_fake_group():
    group_name = fake.sentence(nb_words=3, variable_nb_words=False)
    goals = []

    for i in range(rand.randint(5, 10)):
        goal = create_fake_exercise_goal()
        goals.append(goal)

    group = Group.objects.create(group_name=group_name)

    group.goals.set(goals)

    for i in range(rand.randint(3, 15)):
        user = create_fake_profile()
        group.profile_set.add(user)

    return group
