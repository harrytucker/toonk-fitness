from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from core.models import *
import math


# calculate calories in a meal based off the food and drink attributes when a meal is saved
@receiver(post_save, sender=Meal)
def meal_calorie_handler(sender, instance, created, **kwargs):
    if created:
        # automatically calculate calories in signal

        # check for null instances
        if instance.drink:
            drink_calories_per_100ml = instance.drink.calories_per_100ml
            drink_amount_mls = instance.drink_amount_mls
        else:
            drink_calories_per_100ml = 0
            drink_amount_mls = 0

        # check for null instances
        if instance.food:
            food_calories_per_100g = instance.food.calories_per_100g
            food_amount_grams = instance.food_amount_grams
        else:
            food_calories_per_100g = 0
            food_amount_grams = 0

        instance.calories = drink_calories_per_100ml * drink_amount_mls / 100 + food_calories_per_100g * food_amount_grams / 100

        instance.save()


# calculate calories burnt in an exercise
@receiver(post_save, sender=Exercise)
def exercise_calories_burnt_handler(sender, instance, created, **kwargs):
    if created:
        # round up time in minutes!
        time_in_minutes = math.ceil(instance.duration.total_seconds() / 60)
        instance.calories_burnt = instance.type_of_exercise.calorie_burn_rate_per_minute * time_in_minutes

        instance.save()
