from django.test import TestCase
from .models import *
from django.core.exceptions import *
from faker import Faker
from random import Random
import datetime

# Create your tests here.
# note that setUp does not have a pythonic name, but this is needed as it overrides a TestCase method


class UserTestCase(TestCase):
    """ testing profile handling """
    def setUp(self):
        pass

    def test_create_ten_users(self):
        """ create ten users to see if model is functioning """
        fake = Faker()

        try:
            for i in range(0, 11):
                user = User.objects.create(first_name=fake.first_name(),
                                           last_name=fake.last_name(),
                                           username=fake.user_name(),
                                           email=fake.email(),
                                           password=fake.password())

                user.full_clean()
        except ValidationError:
            self.assertTrue(False)  # error encountered, test failed

        self.assertTrue(True)  # no error, test passed

    def test_submit_incorrect_email(self):
        """ create a user account with an invalid email format """
        fake = Faker()

        with self.assertRaises(ValidationError) as context:
            user = User(first_name=fake.first_name(),
                        last_name=fake.last_name(),
                        username=fake.user_name(),
                        email='this is not an email',
                        password=fake.password())

            user.full_clean()

        self.assertTrue(context.exception is not None)

    def test_submit_incorrect_user_field_types(self):
        fake = Faker()

        with self.assertRaises(ValidationError) as context:
            user = User(first_name=fake.pydict(),
                        last_name=fake.pydict(),
                        username=fake.pydict(),
                        email=fake.name(),
                        password=fake.pydict())

            user.full_clean()

        self.assertTrue(context.exception is not None)

    def test_find_non_existent_user(self):
        """ try to find a user that doesn't exist """
        with self.assertRaises(User.DoesNotExist) as context:
            User.objects.get(pk=999999)

        self.assertTrue(context.exception is not None)

    def test_find_existing_user(self):
        """ try to find a user that does exist """
        fake = Faker()

        user = User.objects.create(first_name="check",
                                   last_name=fake.last_name(),
                                   username=fake.user_name(),
                                   email=fake.email(),
                                   password=fake.password())

        user.full_clean()

        user = User.objects.get(first_name="check")

        self.assertTrue(user is not None)


class MealTestCase(TestCase):
    """ testing meal handling """
    def setUp(self):
        """ create some foods and drinks for testing meals """
        rand = Random()

        for i in range(0, 11):
            food = Food.objects.create(food_name="check" + str(i),
                                       calories_per_100g=Random.uniform(rand, 50, 500))

            food.full_clean()

        for i in range(0, 11):
            drink = Drink.objects.create(drink_name="check" + str(i),
                                         calories_per_100ml=Random.uniform(rand, 50, 500))

            drink.full_clean()

    def test_create_ten_meals(self):
        """ create ten meals to see if model is functioning """
        fake = Faker()

        try:
            for i in range(0, 11):
                meal = Meal.objects.create(meal_name=fake.name(),
                                           date_time_added=django.utils.timezone.now(),
                                           meal_of_the_day='BRE',
                                           food_amount_grams=100.0,
                                           drink_amount_mls=100.0,
                                           calories=100.0)

                meal.food = Food.objects.get(food_name="check" + str(i))
                meal.drink = Drink.objects.get(drink_name="check" + str(i))

                meal.full_clean()
        except ValidationError:
            self.assertTrue(False)

        self.assertTrue(True)

    def test_submit_incorrect_meal_field_types(self):
        fake = Faker()

        with self.assertRaises(TypeError) as context:
            meal = Meal(meal_name=fake.pydict(),
                        date_time_added=fake.pydict(),
                        meal_of_the_day=fake.pydict(),
                        food_amount_grams=fake.pydict(),
                        drink_amount_mls=fake.pydict(),
                        calories=fake.pydict())

            meal.full_clean()

        self.assertTrue(context.exception is not None)

    def test_find_non_existent_meal(self):
        """ try to find a meal that doesn't exist """
        with self.assertRaises(Meal.DoesNotExist) as context:
            Meal.objects.get(pk=999999)

        self.assertTrue(context.exception is not None)

    def test_find_existing_meal(self):
        """ try to find a meal that does exist """
        meal = Meal.objects.create(meal_name="check",
                                   date_time_added=django.utils.timezone.now(),
                                   meal_of_the_day='BRE',
                                   food_amount_grams=100.0,
                                   drink_amount_mls=100.0,
                                   calories=100.0)

        meal.food = Food.objects.get(food_name="check" + str(0))
        meal.drink = Drink.objects.get(drink_name="check" + str(0))

        meal.full_clean()

        meal = Meal.objects.get(meal_name="check")

        self.assertTrue(meal is not None)


class FoodTestCase(TestCase):
    """ testing food handling """
    def setUp(self):
        pass

    def test_create_ten_foods(self):
        """ create ten foods to see if model is functioning """
        fake = Faker()
        rand = Random()

        try:
            for i in range(0, 11):
                food = Food.objects.create(food_name=fake.name(),
                                           calories_per_100g=Random.uniform(rand, 50, 500))

                food.full_clean()
        except ValidationError:
            self.assertTrue(False)

        self.assertTrue(True)

    def test_submit_incorrect_food_field_types(self):
        fake = Faker()

        with self.assertRaises(TypeError) as context:
            food = Food(food_name=fake.pydict(),
                        calories_per_100g=fake.pydict())

            food.full_clean()

        self.assertTrue(context.exception is not None)

    def test_find_non_existent_food(self):
        """ try to find a food that doesn't exist """
        with self.assertRaises(Food.DoesNotExist) as context:
            Food.objects.get(pk=999999)

        self.assertTrue(context.exception is not None)

    def test_find_existing_food(self):
        """ try to find a food that does exist """
        rand = Random()

        food = Food.objects.create(food_name="check",
                                   calories_per_100g=Random.uniform(rand, 50, 500))

        food.full_clean()

        food = Food.objects.get(food_name="check")

        self.assertTrue(food is not None)


class DrinkTestCase(TestCase):
    """ testing drink handling """
    def setUp(self):
        pass
    
    def test_create_ten_drinks(self):
        """ create ten drinks to see if model is functioning """
        fake = Faker()
        rand = Random()

        try:
            for i in range(0, 11):
                drink = Drink.objects.create(drink_name=fake.name(),
                                             calories_per_100ml=Random.uniform(rand, 50, 500))

                drink.full_clean()
        except ValidationError:
            self.assertTrue(False)

        self.assertTrue(True)

    def test_submit_incorrect_drink_field_types(self):
        fake = Faker()

        with self.assertRaises(TypeError) as context:
            drink = Drink(drink_name=fake.pydict(),
                          calories_per_100ml=fake.pydict())

            drink.full_clean()

        self.assertTrue(context.exception is not None)

    def test_find_non_existent_drink(self):
        """ try to find a drink that doesn't exist """
        with self.assertRaises(Drink.DoesNotExist) as context:
            Drink.objects.get(pk=999999)

        self.assertTrue(context.exception is not None)

    def test_find_existing_drink(self):
        """ try to find a drink that does exist """
        rand = Random()

        drink = Drink.objects.create(drink_name="check",
                                     calories_per_100ml=Random.uniform(rand, 50, 500))

        drink.full_clean()

        drink = Drink.objects.get(drink_name="check")

        self.assertTrue(drink is not None)


class ExerciseTypeTestCase(TestCase):
    """ testing exercise type handling """
    def setUp(self):
        pass

    def test_create_ten_exercise_types(self):
        """ create ten exercise types to see if model is functioning """
        fake = Faker()

        try:
            for i in range(0, 11):
                exercise_type = ExerciseType.objects.create(exercise_type_name=fake.name(),
                                                            has_distance=fake.pybool(),
                                                            has_duration=fake.pybool())

                exercise_type.full_clean()
        except ValidationError:
            self.assertTrue(False)

        self.assertTrue(True)

    def test_submit_incorrect_exercise_type_field_types(self):
        fake = Faker()

        with self.assertRaises(TypeError) as context:
            exercise_type = ExerciseType(exercise_type_name=fake.pydict(),
                                         has_distance=fake.pydict(),
                                         has_duration=fake.pydict())

            exercise_type.full_clean()

        self.assertTrue(context.exception is not None)

    def test_find_non_existent_exercise_type(self):
        """ try to find an exercise type that doesn't exist """
        with self.assertRaises(ExerciseType.DoesNotExist) as context:
            ExerciseType.objects.get(pk=999999)

        self.assertTrue(context.exception is not None)

    def test_find_existing_exercise_type(self):
        """ try to find an exercise type that does exist """
        fake = Faker()

        exercise_type = ExerciseType.objects.create(exercise_type_name="check",
                                                    has_distance=fake.pybool(),
                                                    has_duration=fake.pybool())

        exercise_type.full_clean()

        exercise_type = ExerciseType.objects.get(exercise_type_name="check")

        self.assertTrue(exercise_type is not None)


class ExerciseTestCase(TestCase):
    """ testing exercise handling """
    def setUp(self):
        """ create some exercise types for testing exercises """
        fake = Faker()

        for i in range(0, 11):
            exercise_type = ExerciseType.objects.create(exercise_type_name=("check" + str(i)),
                                                        has_distance=fake.pybool(),
                                                        has_duration=fake.pybool())

            exercise_type.full_clean()

    def test_create_ten_exercises(self):
        """ create ten exercises to see if model is functioning """
        fake = Faker()

        try:
            for i in range(0, 11):
                exercise = Exercise.objects.create(exercise_name=fake.name(),
                                                   date_time_added=django.utils.timezone.now(),
                                                   type_of_exercise=(ExerciseType.objects.get(exercise_type_name=("check" + str(i)))),
                                                   duration=datetime.timedelta(minutes=60),
                                                   distance=100)

                exercise.full_clean()

        except ValidationError:
            self.assertTrue(False)

        self.assertTrue(True)

    def test_submit_incorrect_exercise_field_types(self):
        fake = Faker()

        with self.assertRaises(TypeError) as context:
            exercise = Exercise(exercise_name=fake.pydict(),
                                date_time_added=fake.pydict(),
                                type_of_exercise=(ExerciseType.objects.get(exercise_type_name=("check" + str(0)))),
                                duration=fake.pydict(),
                                distance=fake.pydict())

            exercise.full_clean()

        self.assertTrue(context.exception is not None)

    def test_submit_incorrect_exercise_field(self):
        fake = Faker()

        with self.assertRaises(ValueError) as context:
            exercise = Exercise(exercise_name=fake.name(),
                                date_time_added=django.utils.timezone.now(),
                                type_of_exercise=fake.pydict(),
                                duration=datetime.timedelta(minutes=60),
                                distance=100)

            exercise.full_clean()

        self.assertTrue(context.exception is not None)

    def test_find_non_existent_exercise(self):
        """ try to find an exercise that doesn't exist """
        with self.assertRaises(Exercise.DoesNotExist) as context:
            Exercise.objects.get(pk=999999)

        self.assertTrue(context.exception is not None)

    def test_find_existing_exercise(self):
        """ try to find an exercise that does exist """
        fake = Faker()

        exercise_type = ExerciseType.objects.create(exercise_type_name="check",
                                                    has_distance=fake.pybool(),
                                                    has_duration=fake.pybool())

        exercise_type.full_clean()

        exercise = Exercise.objects.create(exercise_name="check",
                                           date_time_added=django.utils.timezone.now(),
                                           type_of_exercise=(ExerciseType.objects.get(exercise_type_name="check")),
                                           duration=datetime.timedelta(minutes=60),
                                           distance=100)

        exercise.full_clean()

        exercise = Exercise.objects.get(exercise_name="check")

        self.assertTrue(exercise is not None)


#Not done yet, copied from exercise test case
class ProfileTestCase(TestCase):
    MALE = 'M'
    FEMALE = 'F'

    GENDER = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    weight_kg = models.FloatField()
    height_m = models.FloatField()
    age = models.IntegerField()
    bmi = models.FloatField(null=True)
    gender = models.CharField(max_length=1, choices=GENDER)
    foods = models.ManyToManyField(Food, blank=True)
    drinks = models.ManyToManyField(Drink, blank=True)
    goals = models.ManyToManyField(Goal, blank=True)
    groups = models.ManyToManyField(Group, blank=True)
    exercises = models.ManyToManyField(Exercise, blank=True)
    meals = models.ManyToManyField(Meal, blank=True)
    history = HistoricalRecords()


    """ testing exercise handling """
    def setUp(self):
        """ create some exercise types for testing exercises """
        fake = Faker()

        for i in range(0, 11):
            exercise_type = ExerciseType.objects.create(exercise_type_name=("check" + str(i)),
                                                        has_distance=fake.pybool(),
                                                        has_duration=fake.pybool())

            exercise_type.full_clean()

    def test_create_ten_exercises(self):
        """ create ten exercises to see if model is functioning """
        fake = Faker()

        try:
            for i in range(0, 11):
                exercise = Exercise.objects.create(exercise_name=fake.name(),
                                                   date_time_added=django.utils.timezone.now(),
                                                   type_of_exercise=(ExerciseType.objects.get(exercise_type_name=("check" + str(i)))),
                                                   duration=datetime.timedelta(minutes=60),
                                                   distance=100)

                exercise.full_clean()

        except ValidationError:
            self.assertTrue(False)

        self.assertTrue(True)

    def test_find_non_existent_exercise(self):
        """ try to find an exercise that doesn't exist """
        with self.assertRaises(Exercise.DoesNotExist) as context:
            Exercise.objects.get(pk=999999)

        self.assertTrue(context.exception is not None)

    def test_find_existing_exercise(self):
        """ try to find an exercise that does exist """
        fake = Faker()

        exercise_type = ExerciseType.objects.create(exercise_type_name="check",
                                                    has_distance=fake.pybool(),
                                                    has_duration=fake.pybool())

        exercise_type.full_clean()

        exercise = Exercise.objects.create(exercise_name="check",
                                           date_time_added=django.utils.timezone.now(),
                                           type_of_exercise=(ExerciseType.objects.get(exercise_type_name="check")),
                                           duration=datetime.timedelta(minutes=60),
                                           distance=100)

        exercise.full_clean()

        exercise = Exercise.objects.get(exercise_name="check")

        self.assertTrue(exercise is not None)

#Not done yet, copied from exercise test case
class GoalTestCase(TestCase):
    """ testing exercise handling """
    def setUp(self):
        """ create some exercise types for testing exercises """
        fake = Faker()

        for i in range(0, 11):
            exercise_type = ExerciseType.objects.create(exercise_type_name=("check" + str(i)),
                                                        has_distance=fake.pybool(),
                                                        has_duration=fake.pybool())

            exercise_type.full_clean()

    def test_create_ten_exercises(self):
        """ create ten exercises to see if model is functioning """
        fake = Faker()

        try:
            for i in range(0, 11):
                exercise = Exercise.objects.create(exercise_name=fake.name(),
                                                   date_time_added=django.utils.timezone.now(),
                                                   type_of_exercise=(ExerciseType.objects.get(exercise_type_name=("check" + str(i)))),
                                                   duration=datetime.timedelta(minutes=60),
                                                   distance=100)

                exercise.full_clean()

        except ValidationError:
            self.assertTrue(False)

        self.assertTrue(True)

    def test_find_non_existent_exercise(self):
        """ try to find an exercise that doesn't exist """
        with self.assertRaises(Exercise.DoesNotExist) as context:
            Exercise.objects.get(pk=999999)

        self.assertTrue(context.exception is not None)

    def test_find_existing_exercise(self):
        """ try to find an exercise that does exist """
        fake = Faker()

        exercise_type = ExerciseType.objects.create(exercise_type_name="check",
                                                    has_distance=fake.pybool(),
                                                    has_duration=fake.pybool())

        exercise_type.full_clean()

        exercise = Exercise.objects.create(exercise_name="check",
                                           date_time_added=django.utils.timezone.now(),
                                           type_of_exercise=(ExerciseType.objects.get(exercise_type_name="check")),
                                           duration=datetime.timedelta(minutes=60),
                                           distance=100)

        exercise.full_clean()

        exercise = Exercise.objects.get(exercise_name="check")

        self.assertTrue(exercise is not None)

#Not done yet, copied from exercise test case
class GroupTestCase(TestCase):
    """ testing exercise handling """
    def setUp(self):
        """ create some exercise types for testing exercises """
        fake = Faker()

        for i in range(0, 11):
            exercise_type = ExerciseType.objects.create(exercise_type_name=("check" + str(i)),
                                                        has_distance=fake.pybool(),
                                                        has_duration=fake.pybool())

            exercise_type.full_clean()

    def test_create_ten_exercises(self):
        """ create ten exercises to see if model is functioning """
        fake = Faker()

        try:
            for i in range(0, 11):
                exercise = Exercise.objects.create(exercise_name=fake.name(),
                                                   date_time_added=django.utils.timezone.now(),
                                                   type_of_exercise=(ExerciseType.objects.get(exercise_type_name=("check" + str(i)))),
                                                   duration=datetime.timedelta(minutes=60),
                                                   distance=100)

                exercise.full_clean()

        except ValidationError:
            self.assertTrue(False)

        self.assertTrue(True)

    def test_find_non_existent_exercise(self):
        """ try to find an exercise that doesn't exist """
        with self.assertRaises(Exercise.DoesNotExist) as context:
            Exercise.objects.get(pk=999999)

        self.assertTrue(context.exception is not None)

    def test_find_existing_exercise(self):
        """ try to find an exercise that does exist """
        fake = Faker()

        exercise_type = ExerciseType.objects.create(exercise_type_name="check",
                                                    has_distance=fake.pybool(),
                                                    has_duration=fake.pybool())

        exercise_type.full_clean()

        exercise = Exercise.objects.create(exercise_name="check",
                                           date_time_added=django.utils.timezone.now(),
                                           type_of_exercise=(ExerciseType.objects.get(exercise_type_name="check")),
                                           duration=datetime.timedelta(minutes=60),
                                           distance=100)

        exercise.full_clean()

        exercise = Exercise.objects.get(exercise_name="check")

        self.assertTrue(exercise is not None)


