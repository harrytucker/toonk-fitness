from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('user', views.user, name='user'),
    path('nutrition', views.nutrition, name='nutrition'),
    path('exercise', views.exercise, name='exercise'),
    path('goal', views.goal, name='goal'),
    path('group', views.group, name='group'),
    path('log-in-user', views.log_in_user, name='log_in_user'),
    path('log-out-user', views.log_out_user, name='log_out_user'),
    path('new-user', views.register_user, name='register_user'),
    # OBJECT INTERACTION PATHS
    path('post/delete', views.delete_goal, name='delete_goal'),
    path('post/mark-finished', views.mark_goal_finished, name='finish_goal'),
    path('post/mark-group-goal-finished', views.mark_group_goal_finished, name='finish_group_goal'),
    path('post/delete-group-goal', views.delete_group_goal, name='delete_group_goal'),
    path('post/leave-group', views.leave_group, name='leave_group'),
    # AJAX/JSON PATHS:
    path('ajax/group-details', views.get_group_details, name='ajax-group-details'),
    path('ajax/goal-details', views.get_goal_details, name='ajax-goal-details'),
]
