from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Food)
admin.site.register(Drink)
admin.site.register(Group)
admin.site.register(ExerciseGoal)
admin.site.register(ExerciseType)
admin.site.register(Exercise)
admin.site.register(WeightGoal)
admin.site.register(Meal)
admin.site.register(Goal)
admin.site.register(Profile)
