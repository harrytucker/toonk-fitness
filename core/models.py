from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import *
import django.utils.timezone
from django.core.exceptions import ValidationError
from simple_history.models import HistoricalRecords


class Food(models.Model):
    """ food model - stores information about an item of food """
    food_name = models.CharField(max_length=50)
    calories_per_100g = models.FloatField()

    def __str__(self):
        return "%s %skCal" % (self.food_name, self.calories_per_100g)

    def clean(self):
        if type(self.food_name) is not str:
            raise TypeError("food_name must be of type str")
        if type(float(self.calories_per_100g)) is not float:
            raise TypeError("calories_per_100g must be of type float")
        if self.calories_per_100g < 0:
            raise ValidationError("calories_per_100g cannot be negative value")


class Drink(models.Model):
    """ drink model - stores information about an item of drink """
    drink_name = models.CharField(max_length=50)
    calories_per_100ml = models.FloatField()

    def __str__(self):
        return "%s %skCal" % (self.drink_name, self.calories_per_100ml)

    def clean(self):
        if type(self.drink_name) is not str:
            raise TypeError("drink_name must be of type str")
        if type(float(self.calories_per_100ml)) is not float:
            raise TypeError("calories_per_100ml must be of type float")
        if self.calories_per_100ml < 0:
            raise ValidationError("calories_per_100ml cannot be negative value")


class ExerciseType(models.Model):
    """ exercise type - differentiates between types of exercise """
    exercise_type_name = models.CharField(max_length=50, unique=True)
    calorie_burn_rate_per_minute = models.FloatField(default=0)
    has_distance = models.BooleanField()
    has_duration = models.BooleanField()

    def __str__(self):
        return "%s" % self.exercise_type_name

    def clean(self):
        if type(self.exercise_type_name) is not str:
            raise TypeError("exercise_type_name must be of type str")
        if type(self.has_distance) is not bool:
            raise TypeError("has_distance must be of type bool")
        if type(self.has_duration) is not bool:
            raise TypeError("has_duration must be of type bool")


class Exercise(models.Model):
    """ exercise model - stores information about a bout of exercise """
    exercise_name = models.CharField(max_length=50)
    date_time_added = models.DateTimeField(default=django.utils.timezone.now)
    type_of_exercise = models.ForeignKey(ExerciseType, on_delete=models.PROTECT)
    calories_burnt = models.FloatField(default=0)
    duration = models.DurationField()
    distance = models.FloatField()
    history = HistoricalRecords()

    def __str__(self):
        return "%s" % self.exercise_name

    def clean(self):
        if type(self.exercise_name) is not str:
            raise TypeError("exercise_name must be of type int")
        if type(self.type_of_exercise) is not ExerciseType:
            raise TypeError("type_of_exercise must be of type ExerciseType")


class WeightGoal(models.Model):
    """ weight goal - describes a weight goal """
    target_weight = models.FloatField()
    need_to_gain_weight = models.BooleanField()

    def __str__(self):
        return "Target Weight: %s" % self.target_weight

    def clean(self):
        if type(self.target_weight) is not float:
            raise TypeError("target_weight must be of type float")
        if self.target_weight < 0:
            raise ValidationError("target_weight cannot be a negative value")
        if type(self.need_to_gain_weight) is not bool:
            raise TypeError("need_to_gain_weight must be of type bool")


class ExerciseGoal(models.Model):
    """ exercise goal model - links to an exercise """
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)

    def __str__(self):
        return "%s" % self.exercise


class Goal(models.Model):
    """ goal model - stores information about a goal """
    goal_name = models.CharField(max_length=50)
    start_date = models.DateTimeField(default=django.utils.timezone.now)
    target_date = models.DateTimeField()
    is_finished = models.BooleanField(default=False)
    is_met = models.BooleanField(default=False)
    weight_goal = models.ForeignKey(WeightGoal, on_delete=models.PROTECT, blank=True, null=True)
    exercise_goal = models.ForeignKey(ExerciseGoal, on_delete=models.PROTECT, blank=True, null=True)
    type_of_exercise = models.ForeignKey(ExerciseType, on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return "%s" % self.goal_name

    def clean(self):
        if type(self.goal_name) is not str:
            raise TypeError("goal_name must be of type str")
        if type(self.is_finished) is not bool:
            # raise TypeError("is_finished must be of type bool")
            self.is_finished = False
        if type(self.is_met) is not bool:
            # raise TypeError("is_met must be of type bool")
            self.is_met = False


class Meal(models.Model):
    """ meal model - stores food and drink information to create a meal """
    BREAKFAST = 'BRE'
    LUNCH = 'LUN'
    DINNER = 'DIN'
    SNACK = 'SNA'

    MEAL_OF_THE_DAY = (
        (BREAKFAST, 'Breakfast'),
        (LUNCH, 'Lunch'),
        (DINNER, 'Dinner'),
        (SNACK, 'Snack'),
    )

    meal_name = models.CharField(max_length=50)
    date_time_added = models.DateTimeField(default=django.utils.timezone.now)
    meal_of_the_day = models.CharField(max_length=3, choices=MEAL_OF_THE_DAY)
    food = models.ForeignKey(Food, blank=True, null=True, on_delete=models.CASCADE)
    food_amount_grams = models.FloatField(default=0)
    drink = models.ForeignKey(Drink, blank=True, null=True, on_delete=models.CASCADE)
    drink_amount_mls = models.FloatField(default=0)
    calories = models.FloatField(default=0)
    history = HistoricalRecords()

    def __str__(self):
        return "%s %skCal" % (self.meal_name, self.calories)

    def clean(self):
        if type(self.meal_name) is not str:
            raise TypeError("meal_name must be of type str")
        if type(self.meal_of_the_day) is not str:
            raise TypeError("meal_of_the_day must be of type str")
        if type(float(self.food_amount_grams)) is not float:
            raise TypeError("food_amount_grams must be of type float")
        if self.food_amount_grams < 0:
            raise ValidationError("food_amount_grams must not be negative")
        if type(float(self.drink_amount_mls)) is not float:
            raise TypeError("drink_amount_mls must be of type float")
        if self.drink_amount_mls < 0:
            raise ValidationError("drink_amount_mls must not be negative")
        if type(float(self.calories)) is not float:
            raise TypeError("calories must be of type float")
        if self.calories < 0:
            raise ValidationError("calories must not be negative")


class Group(models.Model):
    """ user group - a group of users with shared goals """
    group_name = models.CharField(max_length=100, unique=True)
    goals = models.ManyToManyField(Goal, related_name="ongoing_goals_relate", blank=True)

    def __str__(self):
        return "%s" % self.group_name

    def clean(self):
        if type(self.group_name) is not str:
            raise TypeError("group_name must be of type str")


class Profile(models.Model):
    """ profile - extra information regarding user that doesn't involve authentication """
    MALE = 'M'
    FEMALE = 'F'

    GENDER = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    weight_kg = models.FloatField()
    height_m = models.FloatField()
    age = models.IntegerField()
    bmi = models.FloatField(null=True)
    gender = models.CharField(max_length=1, choices=GENDER)
    foods = models.ManyToManyField(Food, blank=True)
    drinks = models.ManyToManyField(Drink, blank=True)
    goals = models.ManyToManyField(Goal, blank=True)
    groups = models.ManyToManyField(Group, blank=True)
    exercises = models.ManyToManyField(Exercise, blank=True)
    meals = models.ManyToManyField(Meal, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return "%s %s" % (self.user.first_name, self.user.last_name)

    '''def clean(self):
        if type(self.weight_kg) is not float:
            raise TypeError("weight_kg must be of type float")
        if self.weight_kg < 0:
            raise ValidationError('Weight may not be less than 0kg')
        if type(self.height_m) is not float:
            raise TypeError("height_m must be of type float")
        if self.height_m < 0:
            raise ValidationError('Height may not be less than 0m')
        if type(self.age) is not int:
            raise TypeError("age must be of type int")
        if self.age < 0:
            raise ValidationError('Age may not be less than 0 years')
        if type(self.bmi) is not float:
            raise TypeError("bmi must be of type float")
        if self.bmi < 0:
            raise ValidationError("bmi must not be negative")
        if type(self.gender) is not str:
            raise TypeError("gender must be of type str")'''
