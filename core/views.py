from django.shortcuts import render
from .models import *
from .forms import *
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import login as auth_login
from datetime import datetime, date
import math
import django.utils.timezone


# Create your views here.

@login_required(login_url='log_in_user')
def index(request):
    """ render the logged in user's dashboard - meals and exercises """
    logged_in_user = request.user
    meal_list = Meal.objects.all()
    exercise_list = Exercise.objects.all()

    today = date.today().day

    today_meal_list = logged_in_user.profile.meals.all()
    today_exercise_list = logged_in_user.profile.exercises.all()

    # for meal in meal_list:
    #     if meal.date_time_added.day == today:
    #         today_meal_list.append(meal)
    #
    # for exercise in exercise_list:
    #     if exercise.date_time_added.day == today:
    #         today_exercise_list.append(exercise)

    completed_goals_list = logged_in_user.profile.goals.all()
    profile_meal_list = logged_in_user.profile.meals.all()
    exercise_list = logged_in_user.profile.exercises.all()
    calories_burnt_list = []
    calories_burnt_dates = []
    calories_burnt_today = 0

    for exercise in exercise_list:
        if exercise.date_time_added.day > datetime.today().day - 7 and exercise.date_time_added.day <= datetime.today().day and exercise.date_time_added.day not in calories_burnt_dates:
            calories_burnt_dates.append(exercise.date_time_added.day)

    for day in calories_burnt_dates:
        calories_burnt = 0
        for exercise in exercise_list:
            if exercise.date_time_added.day == day:
                calories_burnt += exercise.calories_burnt
            if exercise.date_time_added.day == datetime.today().day:
                calories_burnt_today += exercise.calories_burnt
        calories_burnt_list.append(calories_burnt)

    calories_eaten = 0
    for meal in profile_meal_list:
        if meal.date_time_added.day == datetime.today().day:
            calories_eaten += meal.calories

    calorie_allowance = 2000
    if logged_in_user.profile.gender == "M":
        calorie_allowance = 2500

    calories_eaten_of_allowance = int(math.floor(((calories_eaten / calorie_allowance) * 100) / 10.0)) * 10
    if calories_eaten_of_allowance > 100:
        calories_eaten_of_allowance = 100

    context = {'today_meal_list': today_meal_list, 'today_exercise_list': today_exercise_list,
               'logged_in_user': logged_in_user, 'completed_goal_count': len(completed_goals_list),
               'calories_eaten_of_allowance': calories_eaten_of_allowance,
               'calories_burnt_dates': calories_burnt_dates, 'calories_burnt_list': calories_burnt_list,
               'calories_burnt_today': calories_burnt_today}

    return render(request, "index.html", context)


@login_required(login_url='log_in_user')
def user(request):
    """ render user's my details page with forms for updating their profile """
    logged_in_user = request.user

    if request.method == 'POST':
        filled_new_weight = UpdateWeight(request.POST)
        filled_new_height = UpdateHeight(request.POST)
        filled_new_email = UpdateEmail(request.POST)
        filled_new_password = UpdatePassword(request.POST)
        filled_new_target_weight = CreateWeightGoal(request.POST)

        if filled_new_weight.is_valid():
            logged_in_user.profile.weight_kg = filled_new_weight.cleaned_data['new_weight']

            weight_kg = filled_new_weight.cleaned_data['new_weight']
            height_m = logged_in_user.profile.height_m

            logged_in_user.profile.bmi = float(weight_kg / (height_m * height_m))
            logged_in_user.profile.save()

        if filled_new_height.is_valid():
            logged_in_user.profile.height_m = filled_new_height.cleaned_data['new_height']

            weight_kg = logged_in_user.profile.weight_kg
            height_m = filled_new_height.cleaned_data['new_height']

            logged_in_user.profile.bmi = float(weight_kg / (height_m * height_m))
            logged_in_user.profile.save()

        if filled_new_email.is_valid():
            logged_in_user.email = filled_new_email.cleaned_data['new_email']
            logged_in_user.save()

        if filled_new_password.is_valid():
            logged_in_user.password = filled_new_password.cleaned_data['new_password']
            logged_in_user.save()

        if filled_new_target_weight.data.__contains__('target_weight'):
            if filled_new_target_weight.is_valid():
                target = filled_new_target_weight.save()

                target_goal = Goal.objects.create(goal_name='Weight Target', target_date=django.utils.timezone.now(),
                                                  weight_goal=target)

                logged_in_user.profile.goals.add(target_goal)

    target_weight = None
    goals = logged_in_user.profile.goals.all()
    for goal in goals:
        if goal.weight_goal:
            target_weight = goal.weight_goal.target_weight

    user_history = logged_in_user.profile.history.all().order_by('history_date')
    new_weight_form = UpdateWeight()
    new_height_form = UpdateHeight()
    new_email_form = UpdateEmail()
    new_weight_goal_form = CreateWeightGoal()
    new_password_form = UpdatePassword()
    context = {'logged_in_user': logged_in_user,
               'new_weight_form': new_weight_form,
               'new_height_form': new_height_form,
               'new_email_form': new_email_form,
               'new_password_form': new_password_form,
               'target_weight': target_weight,
               'new_weight_goal_form': new_weight_goal_form,
               'user_history': user_history}

    return render(request, "user.html", context)


@login_required(login_url='log_in_user')
def nutrition(request):
    """ render nutrition page, creating foods, drinks, and meals """
    logged_in_user = request.user
    food_creation = False
    drink_creation = False

    if request.method == 'POST':
        filled_food_form = CreateFood(request.POST)
        if filled_food_form.has_changed():
            if filled_food_form.is_valid():
                food = filled_food_form.save()
                logged_in_user.profile.foods.add(food)
                food_creation = True

        filled_drink_form = CreateDrink(request.POST)
        if filled_drink_form.has_changed():
            if filled_drink_form.is_valid():
                drink = filled_drink_form.save()
                logged_in_user.profile.drinks.add(drink)
                drink_creation = True

        filled_meal_form = CreateMeal(request.POST)
        if filled_meal_form['meal_name'] is not '':
            if filled_meal_form.is_valid():
                meal = filled_meal_form.save(commit=False)

                # calories auto-calculated in signals.py module!
                meal.save()

                logged_in_user.profile.meals.add(meal)

    create_food_form = CreateFood()
    create_drink_form = CreateDrink()

    # manual field overrides with CreateMeal() to limit food selections
    create_meal_form = CreateMeal()
    create_meal_form.fields['food'] = forms.ModelChoiceField(Food.objects.filter(profile=logged_in_user.profile))
    create_meal_form.fields['drink'] = forms.ModelChoiceField(Drink.objects.filter(profile=logged_in_user.profile))
    meal_list = logged_in_user.profile.meals.all()
    calorie_list = []
    calorie_dates = []

    """loops through all meals and adds each day that a meal has been logged to calorie_dates"""
    for meal in meal_list:
        if meal.date_time_added.day not in calorie_dates:
            calorie_dates.append(meal.date_time_added.day)

    """loops through the days in calorie_dates and adds the calories of all meals logged on that day, 
    then adds the total for that day to calorie_list"""
    for day in calorie_dates:
        calories = 0
        for meal in meal_list:
            if meal.date_time_added.day == day:
                calories += meal.calories
        calorie_list.append(calories)

    context = {'meal_list': meal_list, 'create_food_form': create_food_form, 'create_drink_form': create_drink_form,
               'create_meal_form': create_meal_form, 'calorie_dates': calorie_dates, 'calorie_list': calorie_list,
               'food_creation': food_creation, 'drink_creation': drink_creation}

    return render(request, "nutrition.html", context)


@login_required(login_url='log_in_user')
def exercise(request):
    """ render exercise page and form """
    logged_in_user = request.user

    if request.method == 'POST':
        filled_exercise_form = CreateExercise(request.POST)
        if filled_exercise_form.is_valid():
            exercise = filled_exercise_form.save()
            logged_in_user.profile.exercises.add(exercise)

    create_exercise_form = CreateExercise()
    exercise_list = logged_in_user.profile.exercises.all()
    calories_burnt_list = []
    calories_burnt_dates = []

    for exercise in exercise_list:
        if exercise.date_time_added.day not in calories_burnt_dates:
            calories_burnt_dates.append(exercise.date_time_added.day)

    for day in calories_burnt_dates:
        calories_burnt = 0
        for exercise in exercise_list:
            if exercise.date_time_added.day == day:
                calories_burnt += exercise.calories_burnt
        calories_burnt_list.append(calories_burnt)

    context = {'exercise_list': exercise_list, 'create_exercise_form': create_exercise_form,
               'calories_burnt_dates': calories_burnt_dates, 'calories_burnt_list': calories_burnt_list}

    return render(request, "exercise.html", context)


@login_required(login_url='log_in_user')
def goal(request):
    """ render goal page and form """
    # get logged in user from session, and use it to get related goals
    logged_in_user = request.user

    if request.method == 'POST':
        filled_goal_form = CreateGoal(request.POST)
        if filled_goal_form.is_valid():
            goal = filled_goal_form.save()
            logged_in_user.profile.goals.add(goal)

    ongoing_goal_list = Goal.objects.filter(profile=logged_in_user.profile, is_finished=False, weight_goal=None)
    completed_goal_list = Goal.objects.filter(profile=logged_in_user.profile, is_finished=True, weight_goal=None)

    if ongoing_goal_list.first():
        next_goal_date = ongoing_goal_list.order_by('target_date').first().target_date
    else:
        next_goal_date = 'No goals set.'

    goals_completed = len(completed_goal_list)
    create_goal_form = CreateGoal()

    context = {'ongoing_goal_list': ongoing_goal_list, 'completed_goal_list': completed_goal_list,
               'create_goal_form': create_goal_form, 'next_goal_date': next_goal_date,
               'goals_completed': goals_completed}

    return render(request, "goal.html", context)


@login_required(login_url='log_in_user')
def group(request):
    """ render group page """
    logged_in_user = request.user

    if request.method == 'POST':
        filled_join_group_form = JoinGroup(request.POST)
        filled_leave_group_form = LeaveGroup(request.POST)
        filled_create_group_form = CreateGroup(request.POST)
        filled_create_group_goal_form = CreateGoal(request.POST)

        if filled_create_group_goal_form.is_valid():
            goal = filled_create_group_goal_form.save()
            group_to_associate = Group.objects.get(pk=request.POST['group_associate_pk'])
            group_to_associate.goals.add(goal)
            group_to_associate.save()

        if filled_create_group_form.is_valid():
            group = filled_create_group_form.save()
            logged_in_user.profile.groups.add(group)
            logged_in_user.profile.save()

        if filled_leave_group_form.is_valid():
            group = Group.objects.get(pk=filled_leave_group_form.cleaned_data['group_id'])

            logged_in_user.profile.groups.remove(group)

        if filled_join_group_form.is_valid():
            group = Group.objects.get(pk=filled_join_group_form.cleaned_data['group_id'])

            if group is not None:
                group.profile_set.add(logged_in_user.profile)
                group.save()

    group_list = logged_in_user.profile.groups.all()
    group_ongoing_goals = []
    group_completed_goals = []
    group_members = []

    for group in group_list:
        group_ongoing_goals = group.goals.filter(is_finished=False)
        group_completed_goals = group.goals.filter(is_finished=True)
        group_members = group.profile_set.all()

    create_group_form = CreateGroup()
    leave_group_form = LeaveGroup()
    join_group_form = JoinGroup()
    create_goal_form = CreateGoal()
    context = {'group_list': group_list, 'create_group_form': create_group_form, 'join_group_form': join_group_form,
               'group_ongoing_goals': group_ongoing_goals, 'group_completed_goals': group_completed_goals,
               'leave_group_form': leave_group_form, 'group_members': group_members,
               'create_goal_form': create_goal_form}

    return render(request, "group.html", context)


def log_in_user(request):
    """ render log-in page and return error messages if filled incorrectly """
    try:
        if request.method == 'POST':
            filled_sign_in_form = SignInUser(request.POST)

            if filled_sign_in_form.is_valid():
                username = filled_sign_in_form.cleaned_data['username']
                password = filled_sign_in_form.cleaned_data['password']
                user = User.objects.get(username=username, password=password)

                if user is not None:
                    # successful login attempt
                    auth_login(request, user)
                    return redirect('/')
                else:
                    return redirect('log_in_user')

        sign_in_form = SignInUser()

        context = {'sign_in_form': sign_in_form, 'sign_in_error': False}

        return render(request, "login.html", context)

    except Exception as e:
        sign_in_form = SignInUser()
        context = {'sign_in_form': sign_in_form, 'sign_in_error': e}
        print(context)

        return render(request, "login.html", context)


@login_required(login_url='log_in_user')
def log_out_user(request):
    """ send request to log out user, then redirect them to the sign-in page """
    auth_logout(request)

    return redirect('log_in_user')


def register_user(request):
    """ render registration page with two combined form objects and handle account creation """
    try:
        if request.method == 'POST':
            filled_user_form = RegisterUser(request.POST)
            filled_profile_form = RegisterProfile(request.POST)

            if filled_user_form.is_valid() and filled_profile_form.is_valid():
                user = filled_user_form.save()
                profile = filled_profile_form.save(commit=False)
                weight_kg = filled_profile_form.cleaned_data['weight_kg']
                height_m = filled_profile_form.cleaned_data['height_m']
                profile.bmi = float(weight_kg / (height_m * height_m))
                profile.user = User.objects.get(pk=user.pk)
                profile.save()

                auth_login(request, user)
                return redirect('/')

        user_form = RegisterUser()
        profile_form = RegisterProfile()

        context = {'user_form': user_form, 'profile_form': profile_form, 'register_error': False}

        return render(request, "new-user.html", context)

    except Exception as e:
        user_form = RegisterUser()
        profile_form = RegisterProfile()

        context = {'user_form': user_form, 'profile_form': profile_form, 'register_error': e}

        return render(request, "new-user.html", context)


# ----------------------------------------------------------------------------------------------------------------
#
#                                           AJAX/JSON VIEWS
#
# ----------------------------------------------------------------------------------------------------------------
def get_group_details(request):
    requested_group_name = request.GET.get('group_name')
    group = Group.objects.filter(group_name=requested_group_name).first()

    data = {
        'group_id': group.pk,
        'group_name': group.group_name,
    }

    i = 0
    for member in group.profile_set.all():
        data[f"user_{i}".format(i)] = member.user.first_name + ' ' + member.user.last_name
        i += 1
    data['member_count'] = i

    ongoing_count = 0
    finished_count = 0
    for goal in group.goals.all():
        if goal.is_finished:
            data[f"completed_goal_{finished_count}".format(i)] = goal.goal_name
            finished_count += 1
        else:
            data[f"ongoing_goal_{ongoing_count}".format(i)] = goal.goal_name
            ongoing_count += 1
    data['ongoing_goals_count'] = ongoing_count
    data['completed_goals_count'] = finished_count

    return JsonResponse(data)


def get_goal_details(request):
    requested_goal_name = request.GET.get('goal_name')
    goal = Goal.objects.filter(goal_name=requested_goal_name).first()

    data = {
        'goal_id': goal.pk,
        'goal_name': goal.goal_name,
        'start_date': goal.start_date,
        'target_date': goal.target_date,
        'is_finished': goal.is_finished,
        'is_met': goal.is_met,
    }

    if goal.type_of_exercise:
        data['type_of_exercise'] = goal.type_of_exercise.exercise_type_name
    else:
        data['type_of_exercise'] = None

    if goal.weight_goal:
        data['weight_goal'] = goal.weight_goal.target_weight
    else:
        data['weight_goal'] = None

    if goal.exercise_goal:
        data['exercise_goal'] = goal.exercise_goal.exercise.exercise_name
    else:
        data['exercise_goal'] = None

    return JsonResponse(data)


# ----------------------------------------------------------------------------------------------------------------
#
#                                           POST REQUESTS ONLY
#
# ----------------------------------------------------------------------------------------------------------------
@login_required(login_url='log_in_user')
def delete_goal(request):
    goal = Goal.objects.filter(pk=request.POST['pk']).first()

    goal.delete()

    return redirect('goal')


@login_required(login_url='log_in_user')
def mark_goal_finished(request):
    goal = Goal.objects.filter(pk=request.POST['pk']).first()

    goal.is_finished = True
    goal.save()

    return redirect('goal')


# alternate redirects for finishing goals within the group page
@login_required(login_url='log_in_user')
def mark_group_goal_finished(request):
    goal = Goal.objects.filter(pk=request.POST['pk']).first()

    goal.is_finished = True
    goal.save()

    return redirect('group')


@login_required(login_url='log_in_user')
def delete_group_goal(request):
    goal = Goal.objects.filter(pk=request.POST['pk']).first()

    goal.delete()

    return redirect('group')


@login_required(login_url='log_in_user')
def leave_group(request):
    group = Group.objects.filter(pk=request.POST['pk']).first()
    logged_in_user = request.user
    group.profile_set.remove(logged_in_user.profile)

    return redirect('group')
