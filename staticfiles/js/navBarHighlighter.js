var url = $(location).attr('href');
var parts = url.split("/");
var last_part = parts[parts.length-1];
var foundActive = false;

$('li a.nav-link').each(function(i, element)
{
    if($(element).attr("href") == last_part)
    {
        $(element).parent().addClass("active");
        foundActive = true;
    }
})

if(!foundActive)
{
    $('#home-nav-link').addClass("active");
}