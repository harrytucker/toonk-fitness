function update(){
    var pos = $(window).scrollTop();
    $('#static-background-image').css('top', pos + 'px');
    $('#static-background-image').css('bottom', -pos + 'px');

    pos = $(window).scrollLeft();
    $('#static-background-image').css('left', pos + 'px');
    $('#static-background-image').css('right', -pos + 'px');
}
$(window).bind('scroll', update);
update();